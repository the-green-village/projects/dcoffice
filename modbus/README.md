# Modbus

https://www.victronenergy.com/live/ccgx:modbustcp_faq

mbpoll is a command line utility to communicate with Modbus slave.

https://github.com/epsilonrt/mbpoll

The slave IDs and the list of registers is in this sheet:
[CCGX-Modbus-TCP-register-list-2.51.xlsx](CCGX-Modbus-TCP-register-list-2.51.xlsx)

For example, reading Input power 1 (register 12, int16) from Venus GX (device instance 261, slave ID 242) can be done like this:

```
mbpoll -p 502 145.94.45.220 -1 -a 242 -r 12 -t 4 -0
```

Example code:
https://github.com/victronenergy/dbus_modbustcp/blob/master/examples/ModbusCli.py
