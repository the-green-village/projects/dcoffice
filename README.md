# DC Office

The DC Office project uses devices from **Victron Energy**.
Real-time data can be obtained over **MQTT** or **Modbus**.

VRM portal: https://vrm.victronenergy.com/installation/17361/dashboard

The table below shows the devices from https://vrm.victronenergy.com/installation/17361/overview

| **Device type** | **Device instance** | **Product** | **Serial** |
| :---- | :---- | :---- | :---- |
| Gateway | | Venus GX | |
| VE.Bus System | 261 | MultiPlus 48/3000/35-16 | |
| Battery Monitor | 288 | BMV-700 | HQ1728VS6QP |
| Battery Monitor | 289 | BMV-700 | HQ1728VNS31 |
| Battery Monitor | 290 | BMV-700 | HQ1728V83D3 |
| Battery Monitor | 291 | BMV-700 | HQ1728VT5PC |
| Battery Monitor | 292 | BMV-700 | HQ1728VTU94 |
| Battery Monitor | 293 | BMV-700 | HQ1728VNLFB |
| Solar Charger | 260 | SmartSolar Charger MPPT 250/60 | HQ1739L33BX |

## VRM API

Victron VRM API is a REST API that can be used to access historical data.
The finest granularity possible is 15 minutes.

https://docs.victronenergy.com/vrmapi/overview.html

https://github.com/victronenergy/vrm-api-python-client

```
export TOKEN=$(curl https://vrmapi.victronenergy.com/v2/auth/login -d '{"username": "'$VRM_USERNAME'", "password": "'$VRM_PASSWORD'"}' | jq -r .token)

#curl https://vrmapi.victronenergy.com/v2/users/47450/installations \
#  -H "X-Authorization: Bearer $TOKEN" | jq .
#curl https://vrmapi.victronenergy.com/v2/users/47450/installations?extended=1 \
#  -H "X-Authorization: Bearer $TOKEN" | jq .
#curl https://vrmapi.victronenergy.com/v2/installations/17361/diagnostics \
#  -H "X-Authorization: Bearer $TOKEN" | jq .

curl https://vrmapi.victronenergy.com/v2/installations/17361/system-overview \
  -H "X-Authorization: Bearer $TOKEN" | jq .
```

Get data in CSV format.

```
curl "https://vrmapi.victronenergy.com/v2/installations/17361/data-download?start=$(($(date +%s)-3600))&end=$(date +%s)&format=csv&datatype=kwh" \
  -H "X-Authorization: Bearer $TOKEN"
curl "https://vrmapi.victronenergy.com/v2/installations/17361/data-download?start=$(($(date +%s)-3600))&end=$(date +%s)&format=csv&datatype=log" \
  -H "X-Authorization: Bearer $TOKEN"
```
