# -*- coding: utf-8 -*-
# Devices
device_list = [
    "solarcharger/260",
    "vebus/261",
    "battery/288",
    "battery/289",
    "battery/290",
    "battery/291",
    "battery/292",
    "battery/293",
]


# Devices
devices = {}

devices["solarcharger/260"] = {
    "device_id": "solarcharger260",
    "device_description": "Solar Charger",
    "device_type": "SmartSolar Charger MPPT 250/60",
    "device_serial": "HQ1739L33BX",
}

devices["vebus/261"] = {
    "device_id": "vebus261",
    "device_description": "VE.Bus System",
    "device_type": "MultiPlus 48/3000/35-16",
}

devices["battery/288"] = {
    "device_id": "battery288",
    "device_description": "Battery Monitor",
    "device_type": "BMW-700",
    "device_serial": "HQ1728VS6QP",
}

devices["battery/289"] = {
    "device_id": "battery289",
    "device_description": "Battery Monitor",
    "device_type": "BMW-700",
    "device_serial": "HQ1728VNS31",
}

devices["battery/290"] = {
    "device_id": "battery290",
    "device_description": "Battery Monitor",
    "device_type": "BMW-700",
    "device_serial": "HQ1728V83D3",
}

devices["battery/291"] = {
    "device_id": "battery291",
    "device_description": "Battery Monitor",
    "device_type": "BMW-700",
    "device_serial": "HQ1728VT5PC",
}

devices["battery/292"] = {
    "device_id": "battery292",
    "device_description": "Battery Monitor",
    "device_type": "BMW-700",
    "device_serial": "HQ1728VTU94",
}

devices["battery/293"] = {
    "device_id": "battery293",
    "device_description": "Battery Monitor",
    "device_type": "BMW-700",
    "device_serial": "HQ1728VNLFB",
}


# Measurements
measurements = {
    "N/985dadcb9763/solarcharger/260/Pv/I": {
        "measurement_id": "Pv/I",
        "measurement_description": "PV Current",
        "unit": "A",
    },
    "N/985dadcb9763/solarcharger/260/Pv/V": {
        "measurement_id": "Pv/V",
        "measurement_description": "PV Voltage",
        "unit": "V",
    },
    "N/985dadcb9763/solarcharger/260/Yield/Power": {
        "measurement_id": "Yield/Power",
        "measurement_description": "PV Yield",
        "unit": "W",
    },
    "N/985dadcb9763/solarcharger/260/Link/VoltageSense": {
        "measurement_id": "Link/VoltageSense",
        "measurement_description": "",
        "unit": "V",
    },
    "N/985dadcb9763/solarcharger/260/Dc/0/Current": {
        "measurement_id": "Dc/0/Current",
        "measurement_description": "DC Current",
        "unit": "A",
    },
    "N/985dadcb9763/solarcharger/260/Dc/0/Voltage": {
        "measurement_id": "Dc/0/Voltage",
        "measurement_description": "DC Voltage",
        "unit": "V",
    },
    "N/985dadcb9763/vebus/261/Bms/AllowToChargeRate": {
        "measurement_id": "Bms/AllowToChargeRate",
        "measurement_description": "",
        "unit": "",
    },
    "N/985dadcb9763/vebus/261/BatterySense/Voltage": {
        "measurement_id": "BatterySense/Voltage",
        "measurement_description": "",
        "unit": "V",
    },
    "N/985dadcb9763/vebus/261/ExtraBatteryCurrent": {
        "measurement_id": "ExtraBatteryCurrent",
        "measurement_description": "",
        "unit": "A",
    },
    "N/985dadcb9763/vebus/261/Dc/0/Voltage": {
        "measurement_id": "Dc/0/Voltage",
        "measurement_description": "DC Voltage",
        "unit": "V",
    },
    "N/985dadcb9763/vebus/261/Dc/0/Current": {
        "measurement_id": "Dc/0/Current",
        "measurement_description": "DC Current",
        "unit": "A",
    },
    "N/985dadcb9763/vebus/261/Dc/0/Power": {
        "measurement_id": "Dc/0/Power",
        "measurement_description": "DC Power",
        "unit": "W",
    },
    "N/985dadcb9763/vebus/261/Ac/ActiveIn/P": {
        "measurement_id": "Ac/ActiveIn/P",
        "measurement_description": "AC Active Input Power",
        "unit": "W",
    },
    "N/985dadcb9763/vebus/261/Ac/ActiveIn/L1/V": {
        "measurement_id": "Ac/ActiveIn/L1/V",
        "measurement_description": "AC Active Input Voltage Phase 1",
        "unit": "V",
    },
    "N/985dadcb9763/vebus/261/Ac/ActiveIn/L1/P": {
        "measurement_id": "Ac/ActiveIn/L1/P",
        "measurement_description": "AC Active Input Power Phase 1",
        "unit": "W",
    },
    "N/985dadcb9763/vebus/261/Ac/Out/P": {
        "measurement_id": "Ac/Out/P",
        "measurement_description": "AC Output Power",
        "unit": "W",
    },
    "N/985dadcb9763/vebus/261/Ac/Out/L1/V": {
        "measurement_id": "Ac/Out/L1/V",
        "measurement_description": "AC Output Voltage Phase 1",
        "unit": "V",
    },
    "N/985dadcb9763/vebus/261/Ac/Out/L1/I": {
        "measurement_id": "Ac/Out/L1/I",
        "measurement_description": "AC Output Current Phase 1",
        "unit": "A",
    },
    "N/985dadcb9763/vebus/261/Ac/Out/L1/P": {
        "measurement_id": "Ac/Out/L1/P",
        "measurement_description": "AC Output Power Phase 1",
        "unit": "W",
    },
    "N/985dadcb9763/vebus/261/Ac/Out/L1/F": {
        "measurement_id": "Ac/Out/L1/F",
        "measurement_description": "AC Output Frequency Phase 1",
        "unit": "Hz",
    },
    "N/985dadcb9763/vebus/261/Energy/AcIn1ToAcOut": {
        "measurement_id": "Energy/AcIn1ToAcOut",
        "measurement_description": "Energy AC Input 1 to AC Output",
        "unit": "kWh",
    },
    "N/985dadcb9763/vebus/261/Energy/AcIn1ToInverter": {
        "measurement_id": "Energy/AcIn1toInverter",
        "measurement_description": "Energy AC Input 1 to Inverter",
        "unit": "kWh",
    },
    "N/985dadcb9763/vebus/261/Energy/AcOutToAcIn1": {
        "measurement_id": "Energy/AcOutToAcIn1",
        "measurement_description": "Energy AC Output to AC Input 1",
        "unit": "kWh",
    },
    "N/985dadcb9763/vebus/261/Energy/InverterToAcIn1": {
        "measurement_id": "Energy/InverterToAcIn1",
        "measurement_description": "Energy Inverter to AC Input 1",
        "unit": "kWh",
    },
    "N/985dadcb9763/vebus/261/Energy/InverterToAcOut": {
        "measurement_id": "Energy/InverterToAcOut",
        "measurement_description": "Energy Inverter to AC Output",
        "unit": "kWh",
    },
    "N/985dadcb9763/vebus/261/Energy/OutToInverter": {
        "measurement_id": "Energy/OutToInverter",
        "measurement_description": "Energy Output to Inverter",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/288/ConsumedAmphours": {
        "measurement_id": "ConsumedAmphours",
        "measurement_description": "Consumed Ah",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/288/Soc": {
        "measurement_id": "Soc",
        "measurement_description": "State of Charge",
        "unit": "%",
    },
    "N/985dadcb9763/battery/288/Dc/0/Voltage": {
        "measurement_id": "Dc/0/Voltage",
        "measurement_description": "DC Voltage",
        "unit": "V",
    },
    "N/985dadcb9763/battery/288/Dc/0/Power": {
        "measurement_id": "Dc/0/Power",
        "measurement_description": "DC Power",
        "unit": "W",
    },
    "N/985dadcb9763/battery/288/Dc/0/Current": {
        "measurement_id": "Dc/0/Current",
        "measurement_description": "DC Current",
        "unit": "A",
    },
    "N/985dadcb9763/battery/288/History/ChargedEnergy": {
        "measurement_id": "History/ChargedEnergy",
        "measurement_description": "Charged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/288/History/DischargedEnergy": {
        "measurement_id": "History/DischargedEnergy",
        "measurement_description": "Discharged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/288/History/TimeSinceLastFullCharge": {
        "measurement_id": "History/TimeSinceLastFullCharge",
        "measurement_description": "Time since last full charge",
        "unit": "s",
    },
    "N/985dadcb9763/battery/288/History/TotalAhDrawn": {
        "measurement_id": "History/TotalAhDrawn",
        "measurement_description": "Total Ah drawn",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/289/ConsumedAmphours": {
        "measurement_id": "ConsumedAmphours",
        "measurement_description": "Consumed Ah",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/289/Soc": {
        "measurement_id": "Soc",
        "measurement_description": "State of Charge",
        "unit": "%",
    },
    "N/985dadcb9763/battery/289/Dc/0/Voltage": {
        "measurement_id": "Dc/0/Voltage",
        "measurement_description": "DC Voltage",
        "unit": "V",
    },
    "N/985dadcb9763/battery/289/Dc/0/Power": {
        "measurement_id": "Dc/0/Power",
        "measurement_description": "DC Power",
        "unit": "W",
    },
    "N/985dadcb9763/battery/289/Dc/0/Current": {
        "measurement_id": "Dc/0/Current",
        "measurement_description": "DC Current",
        "unit": "A",
    },
    "N/985dadcb9763/battery/289/History/ChargedEnergy": {
        "measurement_id": "History/ChargedEnergy",
        "measurement_description": "Charged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/289/History/DischargedEnergy": {
        "measurement_id": "History/DischargedEnergy",
        "measurement_description": "Discharged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/289/History/TimeSinceLastFullCharge": {
        "measurement_id": "History/TimeSinceLastFullCharge",
        "measurement_description": "Time since last full charge",
        "unit": "s",
    },
    "N/985dadcb9763/battery/289/History/TotalAhDrawn": {
        "measurement_id": "History/TotalAhDrawn",
        "measurement_description": "Total Ah drawn",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/290/ConsumedAmphours": {
        "measurement_id": "ConsumedAmphours",
        "measurement_description": "Consumed Ah",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/290/Soc": {
        "measurement_id": "Soc",
        "measurement_description": "State of Charge",
        "unit": "%",
    },
    "N/985dadcb9763/battery/290/Dc/0/Voltage": {
        "measurement_id": "Dc/0/Voltage",
        "measurement_description": "DC Voltage",
        "unit": "V",
    },
    "N/985dadcb9763/battery/290/Dc/0/Power": {
        "measurement_id": "Dc/0/Power",
        "measurement_description": "DC Power",
        "unit": "W",
    },
    "N/985dadcb9763/battery/290/Dc/0/Current": {
        "measurement_id": "Dc/0/Current",
        "measurement_description": "DC Current",
        "unit": "A",
    },
    "N/985dadcb9763/battery/290/History/ChargedEnergy": {
        "measurement_id": "History/ChargedEnergy",
        "measurement_description": "Charged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/290/History/DischargedEnergy": {
        "measurement_id": "History/DischargedEnergy",
        "measurement_description": "Discharged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/290/History/TimeSinceLastFullCharge": {
        "measurement_id": "History/TimeSinceLastFullCharge",
        "measurement_description": "Time since last full charge",
        "unit": "s",
    },
    "N/985dadcb9763/battery/290/History/TotalAhDrawn": {
        "measurement_id": "History/TotalAhDrawn",
        "measurement_description": "Total Ah drawn",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/291/ConsumedAmphours": {
        "measurement_id": "ConsumedAmphours",
        "measurement_description": "Consumed Ah",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/291/Soc": {
        "measurement_id": "Soc",
        "measurement_description": "State of Charge",
        "unit": "%",
    },
    "N/985dadcb9763/battery/291/Dc/0/Voltage": {
        "measurement_id": "Dc/0/Voltage",
        "measurement_description": "DC Voltage",
        "unit": "V",
    },
    "N/985dadcb9763/battery/291/Dc/0/Power": {
        "measurement_id": "Dc/0/Power",
        "measurement_description": "DC Power",
        "unit": "W",
    },
    "N/985dadcb9763/battery/291/Dc/0/Current": {
        "measurement_id": "Dc/0/Current",
        "measurement_description": "DC Current",
        "unit": "A",
    },
    "N/985dadcb9763/battery/291/History/ChargedEnergy": {
        "measurement_id": "History/ChargedEnergy",
        "measurement_description": "Charged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/291/History/DischargedEnergy": {
        "measurement_id": "History/DischargedEnergy",
        "measurement_description": "Discharged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/291/History/TimeSinceLastFullCharge": {
        "measurement_id": "History/TimeSinceLastFullCharge",
        "measurement_description": "Time since last full charge",
        "unit": "s",
    },
    "N/985dadcb9763/battery/291/History/TotalAhDrawn": {
        "measurement_id": "History/TotalAhDrawn",
        "measurement_description": "Total Ah drawn",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/292/ConsumedAmphours": {
        "measurement_id": "ConsumedAmphours",
        "measurement_description": "Consumed Ah",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/292/Soc": {
        "measurement_id": "Soc",
        "measurement_description": "State of Charge",
        "unit": "%",
    },
    "N/985dadcb9763/battery/292/Dc/0/Voltage": {
        "measurement_id": "Dc/0/Voltage",
        "measurement_description": "DC Voltage",
        "unit": "V",
    },
    "N/985dadcb9763/battery/292/Dc/0/Power": {
        "measurement_id": "Dc/0/Power",
        "measurement_description": "DC Power",
        "unit": "W",
    },
    "N/985dadcb9763/battery/292/Dc/0/Current": {
        "measurement_id": "Dc/0/Current",
        "measurement_description": "DC Current",
        "unit": "A",
    },
    "N/985dadcb9763/battery/292/History/ChargedEnergy": {
        "measurement_id": "History/ChargedEnergy",
        "measurement_description": "Charged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/292/History/DischargedEnergy": {
        "measurement_id": "History/DischargedEnergy",
        "measurement_description": "Discharged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/292/History/TimeSinceLastFullCharge": {
        "measurement_id": "History/TimeSinceLastFullCharge",
        "measurement_description": "Time since last full charge",
        "unit": "s",
    },
    "N/985dadcb9763/battery/292/History/TotalAhDrawn": {
        "measurement_id": "History/TotalAhDrawn",
        "measurement_description": "Total Ah drawn",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/293/ConsumedAmphours": {
        "measurement_id": "ConsumedAmphours",
        "measurement_description": "Consumed Ah",
        "unit": "Ah",
    },
    "N/985dadcb9763/battery/293/Soc": {
        "measurement_id": "Soc",
        "measurement_description": "State of Charge",
        "unit": "%",
    },
    "N/985dadcb9763/battery/293/Dc/0/Voltage": {
        "measurement_id": "Dc/0/Voltage",
        "measurement_description": "DC Voltage",
        "unit": "V",
    },
    "N/985dadcb9763/battery/293/Dc/0/Power": {
        "measurement_id": "Dc/0/Power",
        "measurement_description": "DC Power",
        "unit": "W",
    },
    "N/985dadcb9763/battery/293/Dc/0/Current": {
        "measurement_id": "Dc/0/Current",
        "measurement_description": "DC Current",
        "unit": "A",
    },
    "N/985dadcb9763/battery/293/History/ChargedEnergy": {
        "measurement_id": "History/ChargedEnergy",
        "measurement_description": "Charged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/293/History/DischargedEnergy": {
        "measurement_id": "History/DischargedEnergy",
        "measurement_description": "Discharged Energy",
        "unit": "kWh",
    },
    "N/985dadcb9763/battery/293/History/TimeSinceLastFullCharge": {
        "measurement_id": "History/TimeSinceLastFullCharge",
        "measurement_description": "Time since last full charge",
        "unit": "s",
    },
    "N/985dadcb9763/battery/293/History/TotalAhDrawn": {
        "measurement_id": "History/TotalAhDrawn",
        "measurement_description": "Total Ah drawn",
        "unit": "Ah",
    },
    #    "N/985dadcb9763/system/0/Dc/Vebus/Power": {"measurement_id": "", "measurement_description": "", "unit": ""},
    #    "N/985dadcb9763/system/0/Dc/Vebus/Current": {"measurement_id": "", "measurement_description": "", "unit": ""},
    #    "N/985dadcb9763/system/0/Dc/System/Power": {"measurement_id": "", "measurement_description": "", "unit": ""},
    #    "N/985dadcb9763/system/0/Dc/Pv/Current": {"measurement_id": "", "measurement_description": "", "unit": ""},
    #    "N/985dadcb9763/system/0/Dc/Pv/Power": {"measurement_id": "", "measurement_description": "", "unit": ""},
    #    "N/985dadcb9763/system/0/Dc/Battery/Power": {"measurement_id": "", "measurement_description": "", "unit": ""},
    #    "N/985dadcb9763/system/0/Dc/Battery/Current": {"measurement_id": "", "measurement_description": "", "unit": ""},
    #    "N/985dadcb9763/system/0/Dc/Battery/Soc": {"measurement_id": "", "measurement_description": "", "unit": ""},
    #    "N/985dadcb9763/system/0/Ac/Grid/L1/Power": {"measurement_id": "", "measurement_description": "", "unit": ""},
    #    "N/985dadcb9763/system/0/Timers/TimeOnGrid": {"measurement_id": "", "measurement_description": "", "unit": ""},
}
