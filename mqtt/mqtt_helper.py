# -*- coding: utf-8 -*-
import json
import logging

import victron_data

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.WARNING
)
logger = logging.getLogger(__file__)


# Global dictionary to store the latest MQTT messages.
# This dictionary is filled in the on_message function.
mqtt_values = {}


def on_connect(mqttc, obj, flags, rc):
    logger.info("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    mqttc.subscribe("N/985dadcb9763/#", qos=0)


def on_disconnect(mqttc, obj, rc):
    if rc != 0:
        logger.warning("Unexpected disconnection.")


def on_message(mqttc, obj, msg):
    logger.debug(
        "Received message '"
        + str(msg.payload)
        + "' on topic '"
        + msg.topic
        + "' with QoS "
        + str(msg.qos)
    )
    # if msg.retain==1:
    #    logger.info("This is a retained message.")
    msg_json = json.loads(str(msg.payload.decode("utf-8")))

    if msg.topic in victron_data.measurements.keys():
        if not (msg_json["value"] is None):
            # Add the value to the global dictionary for MQTT messages.
            # Previous values for every topic are overwritten.
            mqtt_values[msg.topic] = msg_json["value"]


def on_publish(mqttc, obj, mid):
    logger.info("Messaged ID: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    logger.info("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    logger.log(level, string)
