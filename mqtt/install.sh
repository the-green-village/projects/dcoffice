#!/bin/bash
# mo dcoffice.cfg_template > dcoffice.cfg

kubectl delete configmap dcoffice-mqtt -n tud-gv
kubectl create configmap dcoffice-mqtt -n tud-gv \
  --from-file=dcoffice.cfg=dcoffice.cfg

kubectl delete deploy dcoffice-mqtt -n tud-gv
kubectl apply -f dcoffice-mqtt-dep.yaml
