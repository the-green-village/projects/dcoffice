# -*- coding: utf-8 -*-
import json
import logging
import pathlib
import time
from configparser import ConfigParser

import mqtt_helper
import paho.mqtt.client as mqtt
import victron_data
from confluent_kafka import SerializingProducer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer

if __name__ == "__main__":
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.WARNING,
    )
    logger = logging.getLogger(__file__)

    # Read config file.
    parent_dir = pathlib.Path(__file__).parent.resolve()
    config = ConfigParser()
    config.read(str(parent_dir / "dcoffice.cfg"))
    mqtt_host = config.get("MQTT", "host")
    mqtt_port = config.getint("MQTT", "port")
    bootstrap_servers = config.get("Kafka", "bootstrap_servers")
    api_key = config.get("Kafka", "api_key")
    api_secret = config.get("Kafka", "api_secret")
    topic = config.get("Kafka", "topic")
    schema_registry_url = config.get("Kafka", "schema_registry_url")
    schema_registry_auth = config.get("Kafka", "schema_registry_auth")

    conf = {"url": schema_registry_url, "basic.auth.user.info": schema_registry_auth}
    schema_registry_client = SchemaRegistryClient(conf)

    schema_str = '{"type":"record","name":"GreenVillageRecord","namespace":"thegreenvillage","fields":[{"name":"project_id","type":"string","doc":"Globally unique id for the project."},{"name":"application_id","type":"string","doc":"Unique id for the application/use case."},{"name":"device_id","type":"string","doc":"Unique id for the device."},{"name":"timestamp","type":"long","doc":"Timestamp (milliseconds since unix epoch) of the measurement."},{"name":"measurements","type":{"type":"array","items":{"type":"record","name":"measurement","fields":[{"name":"measurement_id","type":"string","doc":"Unique id for the measurement."},{"name":"value","type":["null","boolean","int","long","float","double","string"],"doc":"Measured value."},{"name":"unit","type":["null","string"],"doc":"Unit of the measurement.","default":null},{"name":"measurement_description","type":["null","string"],"doc":"Measurement description.","default":null}]}},"doc":"Array of measurements."},{"name":"project_description","type":["null","string"],"doc":"Project description.","default":null},{"name":"application_description","type":["null","string"],"doc":"Application/use case description.","default":null},{"name":"device_description","type":["null","string"],"doc":"Device description.","default":null},{"name":"device_manufacturer","type":["null","string"],"doc":"Device manufacturer.","default":null},{"name":"device_type","type":["null","string"],"doc":"Device type.","default":null},{"name":"device_serial","type":["null","string"],"doc":"Device serial number.","default":null},{"name":"location_id","type":["null","string"],"doc":"Unique id for the location.","default":null},{"name":"location_description","type":["null","string"],"doc":"Location description.","default":null},{"name":"latitude","type":["null","float"],"doc":"Latitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"longitude","type":["null","float"],"doc":"Longitude in decimal degrees of the device location (GPS coordinates).","default":null},{"name":"altitude","type":["null","float"],"doc":"Altitude in meters above the mean sea level.","default":null}]}'
    conf = {"auto.register.schemas": False}
    avro_serializer = AvroSerializer(schema_registry_client, schema_str, conf=conf)

    # Create Producer instance
    producer = SerializingProducer(
        {
            "client.id": "dcoffice-producer",
            "bootstrap.servers": bootstrap_servers,
            "sasl.mechanisms": "PLAIN",
            "security.protocol": "SASL_SSL",
            "sasl.username": api_key,
            "sasl.password": api_secret,
            "ssl.ca.location": "/etc/ssl/certs/ca-certificates.crt",
            "value.serializer": avro_serializer,
            "acks": "1",
            # If you don’t care about duplicates and ordering:
            "retries": 10000000,
            "delivery.timeout.ms": 2147483647,
            "max.in.flight.requests.per.connection": 5
            #        # If you don’t care about duplicates but care about ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'max.in.flight.requests.per.connection': 1
            #        # If you care about duplicates and ordering:
            #        'retries': 10000000,
            #        'delivery.timeout.ms': 2147483647,
            #        'enable.idempotence': True
        }
    )

    delivered_records = 0

    # Optional per-message on_delivery handler (triggered by poll() or flush())
    # when a message has been successfully delivered or
    # permanently failed delivery (after retries).
    def acked(err, msg):
        global delivered_records
        """Delivery report handler called on
        successful or failed delivery of message
        """
        if err is not None:
            logger.error("Failed to deliver message: {}".format(err))
        else:
            delivered_records += 1
            logger.info(
                "Produced record to topic {} partition [{}] @ offset {}".format(
                    msg.topic(), msg.partition(), msg.offset()
                )
            )
        
    # MQTT client
    mqttc = mqtt.Client()
    mqttc.enable_logger(logger)

    mqttc.on_message = mqtt_helper.on_message
    mqttc.on_connect = mqtt_helper.on_connect
    mqttc.on_publish = mqtt_helper.on_publish
    mqttc.on_subscribe = mqtt_helper.on_subscribe

    mqttc.connect(mqtt_host, mqtt_port, 60)
    mqttc.loop_start()

    try:
        while True:
            # Send keep-alive message.
            # Default keep-alive interval is 60 seconds. If the CCGX does not receive any read or write requests during that interval,
            # the notifications will be stopped, until the next read or write request is received.
            mqttc.publish("R/985dadcb9763/system/0/Serial", "")

            # Initialize empty dictionaries for the values section in the messages
            # in the generic schema format for each device.
            data_values = {device: [] for device in victron_data.device_list}

            # Current time in milliseconds
            timestamp = int(time.time() * 1e3)

            # Turn the values received in MQTT messages into the generic schema format.
            for k, v in mqtt_helper.mqtt_values.items():
                # mqtt_values uses the MQTT topic as a key.
                # From the MQTT topic, get the device index used as a key in the measurements dictionary.
                topic_list = k.split("/")
                device = f"{topic_list[2]}/{topic_list[3]}"

                # Get the measurement fields and add value.
                data_value = victron_data.measurements[k]
                data_value["value"] = float(v)

                # Append to the list of values.
                data_values[device].append(data_value)

            # Send a message to Kafka for each device.
            for device in victron_data.device_list:
                # Skip empty messages.
                if not len(data_values[device]):
                    continue

                message = victron_data.devices[device]
                message["project_id"] = "dcoffice"
                message["application_id"] = "victron"
                message["timestamp"] = timestamp
                message["measurements"] = data_values[device]
                logger.debug(json.dumps(message, sort_keys=True, indent=4))

                producer.produce(topic=topic, value=message, on_delivery=acked)

            # Empty the global dictionary for MQTT messages.
            mqtt_helper.mqtt_values = {}

            # Trigger any available delivery report callbacks from previous produce() call.
            producer.poll(0)

            # Sleep for a bit so we don't flood the cluster.
            time.sleep(1)

    except KeyboardInterrupt:
        logger.error("Aborted by user")

    finally:
        producer.flush()

    logger.info("{} messages were produced to topic {}!".format(delivered_records, topic))
