# MQTT

https://github.com/victronenergy/dbus-mqtt

https://www.victronenergy.com/live/venus-gx:start

Data is transmitted only after keep-alive messages are sent.

```
while :; do mosquitto_pub -h $MQTT_HOST -m '' -t 'R/985dadcb9763/system/0/Serial'; sleep 5; done
```

MQTT topic format: `N/<portal ID>/<service_type>/<device instance>/<D-Bus path>`

```
mosquitto_sub -v -h $MQTT_HOST -t 'N/985dadcb9763/+/+/ProductId'
mosquitto_sub -v -h $MQTT_HOST -t 'N/985dadcb9763/#'
```

## Available data

Keep consuming messages for a while and see what variables are available.

```
mosquitto_sub -v -h $MQTT_HOST -t 'N/985dadcb9763/#' > mqtt.log
awk '{print $1}' mqtt.log | sort -u

awk '{print $1}' mqtt.log | sort -u | grep '/solarcharger/260/' > solarcharger.txt
awk '{print $1}' mqtt.log | sort -u | grep '/vebus/261/' > vebus.txt
awk '{print $1}' mqtt.log | sort -u | grep '/battery/288/' > battery.txt
awk '{print $1}' mqtt.log | sort -u | grep '/system/0/' > system.txt
```

```
mosquitto_sub -v -h $MQTT_HOST -t 'N/985dadcb9763/solarcharger/260/#'
mosquitto_sub -v -h $MQTT_HOST -t 'N/985dadcb9763/vebus/261/#'
mosquitto_sub -v -h $MQTT_HOST -t 'N/985dadcb9763/battery/288/#'
mosquitto_sub -v -h $MQTT_HOST -t 'N/985dadcb9763/system/0/#'
```

## Python MQTT client and Kafka producer

### Run from Raspberry Pi (deprecated)

```
wget https://github.com/edenhill/librdkafka/archive/v0.11.6.tar.gz
tar xf v0.11.6.tar.gz
cd librdkafka-0.11.6
./configure
make
sudo make install

export LD_LIBRARY_PATH=/usr/local/lib/
sudo ldconfig
```

### Run with Docker

```
export DOCKER_ID_USER="salekd"
docker login https://index.docker.io/v1/

docker build . -t dcoffice-mqtt
docker tag dcoffice-mqtt $DOCKER_ID_USER/dcoffice-mqtt:1.0.0
docker push $DOCKER_ID_USER/dcoffice-mqtt:1.0.0

docker run -v $(pwd)/dcoffice.cfg:/dcoffice.cfg dcoffice-mqtt
```

### Deploy on Kubernetes

```
# Create ConfigMap with the configuration file.
kubectl delete configmap dcoffice-mqtt -n ccloud
kubectl create configmap dcoffice-mqtt -n ccloud \
  --from-file=dcoffice.cfg=dcoffice.cfg

# Deploy a pod.
kubectl delete deploy dcoffice-mqtt -n ccloud
kubectl apply -f dcoffice-mqtt-dep.yaml

kubectl logs deploy/dcoffice-mqtt -n ccloud
```
